var express = require('express');
var router = express.Router();

var request = require('request'),
    async = require('async');

/**
 * Home Page
 */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/**
 * API for getting account list
 */
router.get('/a/accounts', function(req, res, next){

  var token = 'b4454c70c019575a90f92b092cf1c37f';
  var url = 'https://crm.zoho.com/crm/private/json/Accounts/getRecords?newFormat=1&scope=crmapi&authtoken=' + token;

  request(url, function(err, response, body){
    if(err) console.log(err.stack);

    try {
      var o = JSON.parse(body);
      var items = o.response.result.Accounts.row;

      var result = [];
      items.forEach(function(item){

        var data = {};
        item['FL'].forEach(function(field){
          data[field.val] = field.content;
        });
        result.push(data);
      });

      res.json(result);

    } catch (err) {
      console.log(err.stack);
    }
  });
});

/**
 * API(mock) to submit data
 */
router.post('/a/submit_data', function(req, res){
  res.json({status:'success'});
});

module.exports = router;
