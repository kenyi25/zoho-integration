var request = require('request'),
    async = require('async');

var token = 'b4454c70c019575a90f92b092cf1c37f';
var url = 'https://crm.zoho.com/crm/private/json/Accounts/getRecords?newFormat=1&scope=crmapi&authtoken=' + token;

request(url, function(err, response, body){
    if(err) console.log(err.stack);

    try {
        var o = JSON.parse(body);
        var items = o.response.result.Accounts.row;

        var result = [];
        items.forEach(function(item){

            var data = {};
            item['FL'].forEach(function(field){
                data[field.val] = field.content;
            });
            result.push(data);
        });

        console.log(JSON.stringify(result, null, 2));

    } catch (err) {
        console.log(err.stack);
    }
});